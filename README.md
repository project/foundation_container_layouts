# ZURB Foundation Container Layouts

## Introduction
The module defines layouts using the Layout API mainly for ZURB Foundation Container elements.

The following layouts are available:
- 2 columns
- 3 columns
- 4 columns
- Accordion (https://get.foundation/sites/docs/accordion.html)
- Card (https://get.foundation/sites/docs/card.html)
- Tabs (https://get.foundation/sites/docs/tabs.html)
- Responsive Accordion Tabs (https://get.foundation/sites/docs/responsive-accordion-tabs.html)
- Orbit (https://get.foundation/sites/docs/orbit.html)

Layouts that are based on the Foundation Container elements can be useful when working with the contrib Bricks module.
That way, you can build an Accordion easily with Brick entities as accordion items for example.

## Install
### Requirements:
- Drupal core 8.7.7 or higher
- ZURB Foundation 6 theme (https://www.drupal.org/project/zurb_foundation)
- (Optional) Bricks (https://www.drupal.org/project/bricks)

### Installation:
- Install the module with composer or manually. For more information, visit https://www.drupal.org/docs/extending-drupal/installing-modules
- If you want to use it with Bricks, make sure to enable modules Bricks, Bricks Default, Bricks Inline, Entity Construction Kit, Inline Entity Form, Layout Discovery.

## Usage
You can use the layouts if you enable Layout Builder for a content.
That way only the 2 columns, 3 columns, 4 columns layouts will be useful. It's because the Layout Builder uses mainly blocks and fields. If you want to build an Accordion that way, then the accordion items can be only separate fields or blocks.

With the Bricks module:
- create a new Bricky content,
- add a Layout brick,
- select the layout you want to use,
- add Text, Image etc. brick with your content. Make sure that every content is nested under the Layout brick.

If you don't see the layouts in the list, clear the caches and try again.
